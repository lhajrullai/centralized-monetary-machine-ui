# Centralized Monetary Machine (UI)

User interface for Centralized Monetary Machine (Server)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
NodeJS v8.9 or greater
Angular CLI

```

### Installing

A step by step series of examples that tell you how to get a development env running

```
npm install
ng build --prod
```

## Deployment

Add additional notes about how to deploy this on a live system

```
tsc server.ts
node server.js
```
## Built With

* [AngularCli](https://cli.angular.io/) - The web framework used
* [NPM](https://www.npmjs.com/) - Package Manager

## Authors

* **Lorik Hajrullai** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
