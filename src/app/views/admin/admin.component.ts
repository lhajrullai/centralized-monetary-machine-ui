import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../services/admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  adminData: any;
  state: string;
  type: string;
  result: any;
  errorMessage: string;

  constructor(private adminService: AdminService) {
    this.adminData = {};
    this.state = 'init';
  }


  executeInit(data: any) {
    this.type = 'loading';
    this.adminService.initializeFunds(data).subscribe(result => {
      this.type = 'result';
      this.result = result;
    }, error => {
      this.type = 'error';
      this.errorMessage = error.error.message;
    });
  }

  executeReset(data: any) {
    this.type = 'loading';
    this.adminService.resetFunds(data).subscribe(result => {
      this.type = 'result';
      this.result = result;
    }, error => {
      this.type = 'error';
      this.errorMessage = error.error.message;
    });
  }

  getFunds() {
    this.type = 'loading';
    this.adminService.retrieveFunds().subscribe(result => {
      this.type = 'result';
      this.result = result;
    }, error => {
      this.type = 'error';
      this.errorMessage = error.error.message;
    });
  }

  onSubmit(data: any) {
    switch (this.state) {
      case 'init':
        this.executeInit(data);
        break;
      case 'reset':
        this.executeReset(data);
        break;
    }
  }

  changeState(state: string) {
    this.state = state;
    this.type = '';

    if (state === 'show') {
      this.getFunds();
    }

  }

  ngOnInit() {
  }

}
