///<reference path="../../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import {Component, OnDestroy, OnInit} from '@angular/core';
import {WithdrawalService} from '../../services/withdrawal.service';

@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class WithdrawalComponent implements OnInit, OnDestroy {

  withdrawalData: any;
  loading: boolean;
  type: string;
  errorMessage: string;
  result: any;

  constructor(private withdrawalService: WithdrawalService) {
    this.withdrawalData = {};
    this.loading = false;
    this.type = '';
    this.result = {};
  }

  ngOnInit() {
  }

  retry() {
    this.type = '';
  }

  executeWithdraw(withdraw: any) {
    this.type = 'loading';
    this.withdrawalService.withdraw(withdraw).subscribe(result => {
      this.type = 'result';
      this.result = result;
    }, error => {
      this.type = 'error';
      this.errorMessage = error.error.message;
    });
  }

  onSubmit(withdraw: any) {
    this.executeWithdraw(withdraw);
  }

  ngOnDestroy() {
  }
}
