import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {constants} from '../app-constants';

@Injectable({
  providedIn: 'root'
})
export class WithdrawalService {

  constructor(private http: HttpClient) {
  }

  withdraw(withdraw: any): Observable<any> {
    return this.http.post(constants.base_url_public + '/withdraw', withdraw);
  }

}
