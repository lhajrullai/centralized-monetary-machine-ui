import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {constants} from '../app-constants';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) {
  }

  initializeFunds(init: any): Observable<any> {
    return this.http.post(constants.base_url_admin + '/funds', init);
  }

  resetFunds(init: any): Observable<any> {
    return this.http.put(constants.base_url_admin + '/funds', init);
  }

  retrieveFunds() {
    return this.http.get(constants.base_url_admin + '/funds');
  }

}
