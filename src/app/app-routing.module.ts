import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WithdrawalComponent} from './views/withdrawal/withdrawal.component';
import {AdminComponent} from './views/admin/admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent
  },
  {
    path: 'withdrawal',
    component: WithdrawalComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
